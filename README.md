# aztech/event-bus-extra-plugins

## Build status

[![Build Status](https://travis-ci.org/aztech-dev/event-bus-extra-plugins.png?branch=master)](https://travis-ci.org/aztech-dev/event-bus-extra-plugins)
[![Code Coverage](https://scrutinizer-ci.com/g/aztech-dev/event-bus-extra-plugins/badges/coverage.png?b=master)](https://scrutinizer-ci.com/g/aztech-dev/event-bus-extra-plugins/?branch=master)
[![Scrutinizer Quality Score](https://scrutinizer-ci.com/g/aztech-dev/event-bus-extra-plugins/badges/quality-score.png?s=668e4df5ba163c804504257d4a026a0a549f220a)](https://scrutinizer-ci.com/g/aztech-dev/event-bus-extra-plugins/)
[![Dependency Status](https://www.versioneye.com/user/projects/53b92a84609ff04f7f000003/badge.svg)](https://www.versioneye.com/user/projects/53b92a84609ff04f7f000003)
[![HHVM Status](http://hhvm.h4cc.de/badge/aztech/event-bus-extra-plugins.png)](http://hhvm.h4cc.de/package/aztech/event-bus-extra-plugins)

## Stability

[![Latest Stable Version](https://poser.pugx.org/aztech/event-bus-extra-plugins/v/stable.png)](https://packagist.org/packages/aztech/event-bus-extra-plugins)
[![Latest Unstable Version](https://poser.pugx.org/aztech/event-bus-extra-plugins/v/unstable.png)](https://packagist.org/packages/aztech/event-bus-extra-plugins)

## Installation

### Via Composer

Composer is the only supported way of installing *aztech/event-bus-extra-plugins* . Don't know Composer yet ? [Read more about it](https://getcomposer.org/doc/00-intro.md).


`$ composer require "aztech/event-bus-extra-plugins":"~1"`

## Autoloading

Add the following code to your bootstrap file :

```
require_once 'vendor/autoload.php';
```
